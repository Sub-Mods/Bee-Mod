package com.fokbees.beez.proxy;

import com.fokbees.beez.Beez;
import com.fokbees.beez.common.entities.EntityBee;
import com.fokbees.beez.common.init.EntityReg;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;

public class ClientProxy extends CommonProxy
{
	@Override
	@EventHandler
	public void init(FMLInitializationEvent event) {
		System.out.println("Renders");
		EntityReg.Rendering();
		EntityRegistry.registerModEntity(new ResourceLocation(Beez.MODID,"Bee"), EntityBee.class, "Bee", 34, Beez.instance, 80, 3, false);	}

	}


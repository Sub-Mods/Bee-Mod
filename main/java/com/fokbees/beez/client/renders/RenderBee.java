package com.fokbees.beez.client.renders;

import com.fokbees.beez.Beez;
import com.fokbees.beez.common.entities.EntityBee;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class RenderBee extends RenderLiving<EntityBee>
{

	private static final ResourceLocation BeeTexture = new ResourceLocation(Beez.MODID, "textures/models/entities/bee.png");
	

	public RenderBee(RenderManager rendermanagerIn, ModelBase modelbaseIn, float shadowsizeIn) 
	{
		super(rendermanagerIn, modelbaseIn, shadowsizeIn);
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityBee entity) 
	{
		return BeeTexture;
	}
	
}

package com.fokbees.beez.common.init;

import com.fokbees.beez.Beez;
import com.fokbees.beez.client.models.ModelBee;
import com.fokbees.beez.client.renders.RenderBee;
import com.fokbees.beez.common.entities.EntityBee;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelZombie;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class EntityReg 
{
	public static void Rendering()
	{
		RenderingRegistry.registerEntityRenderingHandler(EntityBee.class, new RenderBee(Minecraft.getMinecraft().getRenderManager(), new ModelBee(), 0.5F));
	}
}
